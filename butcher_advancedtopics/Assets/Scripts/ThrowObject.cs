﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowObject : MonoBehaviour
{
    public GameObject projectile;
    public AudioClip shootSound;
    // variable type referring to any audio file.

    private float throwSpeed = 2000f;
    private AudioSource source;
    private float volLowRange = 0.5f;
    private float volHiRange = 1.0f;
    // sets volume limits to have a range
    //      of sounds

    

    // Start is called before the first frame update
    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            float vol = Random.Range(volLowRange, volHiRange);
            // our volume is now random between our two parameters

            source.PlayOneShot(shootSound, vol);
            // PlayOneShot plays once, takes a sound clip and a volume.

            GameObject throwThis = Instantiate(projectile, transform.position, transform.rotation);
            //create new game obj. instantiate a projectile at / with this obj's pos & rotation.

            throwThis.GetComponent<Rigidbody>().AddRelativeForce(new Vector3(0, 0, throwSpeed));
        }
    }

}
